import com.hazelcast.aggregation.Aggregators;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.Config;
import com.hazelcast.core.EntryView;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;

import java.net.UnknownHostException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.toMap;

public class HClientMethods {

    ClientConfig clientConfig;
    Config config;
    HazelcastInstance client;


    public void ConfigureClient() throws UnknownHostException {
        clientConfig = HConfig.getClientConfig();
        client = HazelcastClient.newHazelcastClient(clientConfig);
    }

    public void ConfigureServer() throws UnknownHostException {
        config = HConfig.getConfig();
        client = Hazelcast.newHazelcastInstance(config);
    }

    public void CloseConnection() {
        client.getLifecycleService().shutdown();
    }

    public void HPutStudent(String imie, String surname, long pesel, int dataUr, int grupa_id) {
        IMap<Long, Student> students = client.getMap("students");
        Map<Long, Student> sStudents = new TreeMap<Long, Student>(students);
        IMap<Long, Grupa> grupy = client.getMap("grupy");
        Grupa grupa = grupy.get(Long.valueOf(grupa_id));
        if (grupa != null) {
            long key = 0;
            for (Entry<Long, Student> e : sStudents.entrySet()) {
                key = e.getKey();
            }

            Student _student = new Student(imie, surname, pesel, dataUr, grupa_id);
            students.put(++key, _student);
            System.out.println("PUT " + key + " => " + _student);
        } else
            System.out.println("Nieprawidłowe Id grupy");
    }

    public void HGetStudent() {
        IMap<Long, Student> students = client.getMap("students");
        long key = students.size() + 1;
        System.out.println("All students: ");
        for (Entry<Long, Student> e : students.entrySet()) {
            System.out.println(e.getKey() + " => " + e.getValue());
        }
    }

    public void HGetStudent(int i) {
        IMap<Long, Student> students = client.getMap("students");
        long key = students.size() + 1;
        System.out.println("Student with key:" + i);
        for (Entry<Long, Student> e : students.entrySet()) {
            if (e.getKey() == i) {
                System.out.println(e.getKey() + " => " + e.getValue());
            }
        }
    }

    public void HGetStudent(String s) {
        IMap<Long, Student> students = client.getMap("students");
        long key = students.size() + 1;

        Predicate<Long, Student> pred = Predicates.equal("name", s);
        Collection<Student> _students = students.values(pred);
        System.out.println("Student z nazwiskiem: " + s);
        for (Student _s : _students) {
            System.out.println(_s);
        }
    }

    public void HEditStudent(long i, String name, String surname, int grupa){
        IMap<Long, Student> students = client.getMap("students");
        Student student = students.get(i);
        student.setName(name);
        student.setSurname(surname);
        student.setGrupa(grupa);
        students.put(i, student);
    }

    public void HEvictStudent(long key) {
        IMap<Long, Student> students = client.getMap("students");
        System.out.println("Deleted " + key + " => " + students.get(key));
        students.evict(key);
    }

    public void HEvictAllStudent() {
        IMap<Long, Student> students = client.getMap("students");
        students.evictAll();
        System.out.println("Wszyscy studenci zostali usunięci");
    }

    public void HPutGroup(String nazwa, int rocznik) {
        IMap<Long, Grupa> grupy = client.getMap("grupy");
        Map<Long, Grupa> sGrupy = new TreeMap<Long, Grupa>(grupy);
        long key = 0;
        for (Entry<Long, Grupa> e : sGrupy.entrySet()) {
            key = e.getKey();
        }
        Grupa _grupa = new Grupa(nazwa, rocznik);
        grupy.put(++key, _grupa);
        System.out.println("PUT " + key + " => " + _grupa);

    }

    public void HGetGroup() {
        IMap<Long, Grupa> grupy = client.getMap("grupy");
        long key = grupy.size() + 1;
        System.out.println("Wszystkie grupy: ");
        for (Entry<Long, Grupa> e : grupy.entrySet()) {
            System.out.println(e.getKey() + " => " + e.getValue());
        }
    }

    public void HGetAllStudentsFromGroup(int grupa_id) {
        IMap<Long, Student> students = client.getMap("students");
        Collection<Student> _students = new LinkedList<Student>();
        for (Entry<Long, Student> s : students.entrySet()) {
            if (s.getValue().getGrupa() == grupa_id)
                _students.add(s.getValue());
        }

        System.out.println("Wszyscy studenci z wybranej grupy");
        for (Student s : _students) {
            System.out.println(s);
        }
    }


}
