import java.io.Serializable;

public class Grupa implements Serializable {
    private static final long serialVersionUID = 2L;

    private String Nazwa;
    private int rocznik;

    public Grupa(String nazwa, int rocznik) {
        Nazwa = nazwa;
        this.rocznik = rocznik;
    }

    public void setNazwa(String nazwa) {
        Nazwa = nazwa;
    }

    public void setRocznik(int rocznik) {
        this.rocznik = rocznik;
    }

    public String getNazwa() {
        return Nazwa;
    }

    public int getRocznik() {
        return rocznik;
    }

    @Override
    public String toString(){
        return "Grupa " + Nazwa + " - Rocznik: " + rocznik;
    }
}
